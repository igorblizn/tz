package qweAA.steps;


import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import qweAA.pages.MainPage;



public class StepDefinitions {
    WebDriver driver;
    MainPage mainPage;

    @Given("^I open Google$")
    public void i_open_Google() throws Throwable {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        mainPage = new MainPage(driver);
        driver.get("https://google.com");
    }

    @When("^Try to search \"(.*)\"$")
    public void tru_to_search_googles(String text) throws Throwable {
        mainPage.fillSearchField(text);
        //mainPage.submitSearchField();
        mainPage.clickSearchButton();
    }

    @Then("^Search result is OK$")
    public void search_result_is_OK() throws Throwable {
        mainPage.shouldBeResult();
        System.out.println("qwqw");

    }

    @After
    public void close(){
        driver.quit();
    }

}
