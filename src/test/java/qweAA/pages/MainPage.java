package qweAA.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class MainPage {
    private WebDriver webDriver;
    private WebDriverWait wait;

    public MainPage(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 5);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//*[@name='q']")
    WebElement searchField;

    @FindBy(xpath = "//*[@value='Поиск в Google']")
    WebElement searchButton;

    @FindBy(id = "resultStats")
    WebElement resultStatus;

    public void fillSearchField(String searchRequest) {
        System.out.println("start wait");
        wait.until(ExpectedConditions.visibilityOf(searchField));
        System.out.println("stop wait");
        searchField.sendKeys(searchRequest);
    }

    public void submitSearchField(){
        searchField.submit();
    }

    public void clickSearchButton() {
        wait.until(ExpectedConditions.visibilityOf(searchButton));
        searchButton.click();
    }

    public void shouldBeResult(){
        wait.until(ExpectedConditions.visibilityOf(resultStatus));
        assertTrue(resultStatus.isDisplayed());
    }
}
